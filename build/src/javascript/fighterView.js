import { createElement } from "./helpers/domHelper";
import { showFighterDetails, createFightersSelector } from "./fightersView";
export function createFighter(fighter) {
    let name = fighter.name;
    let source = fighter.source;
    const nameElement = createName(name);
    const imageElement = createImage(source);
    const checkboxElement = createCheckbox(source);
    const fighterContainer = createElement("div", "fighter");
    fighterContainer.append(imageElement, nameElement, checkboxElement);
    const preventCheckboxClick = (ev) => ev.stopPropagation();
    const onCheckboxClick = (ev) => createFightersSelector(ev, fighter);
    const onFighterClick = (ev) => showFighterDetails(ev, fighter);
    fighterContainer.addEventListener("click", onFighterClick, false);
    checkboxElement.addEventListener("change", onCheckboxClick, false);
    checkboxElement.addEventListener("click", preventCheckboxClick, false);
    return fighterContainer;
}
function createName(name) {
    const nameElement = createElement("span", "name");
    nameElement.innerText = name;
    return nameElement;
}
function createImage(source) {
    const attributes = { src: source };
    const imgElement = createElement("img", "fighter-image", attributes);
    return imgElement;
}
function createCheckbox(source) {
    const label = createElement("label", "custom-checkbox");
    const span = createElement("span", "checkmark");
    const attributes = { type: "checkbox" };
    const checkboxElement = createElement("input", null, attributes);
    label.append(checkboxElement, span);
    return label;
}
