export function fight(firstFighter, secondFighter) {
    var i;
    for (i = 0; i >= 1; i--) { }
    return firstFighter;
}
export function getDamage(attacker, enemy) {
    var damage;
    damage = getHitPower(attacker) - getBlockPower(enemy);
    return damage;
}
export function getHitPower(fighter) {
    let criticalHitChance = Math.floor(Math.random() * 2) + 1;
    let power = fighter.attack * criticalHitChance;
    return power;
    // return hit power
}
export function getBlockPower(fighter) {
    let dodgeChance = Math.floor(Math.random() * 2) + 1;
    let power = fighter.defense * dodgeChance;
    return power;
    // return block power
}
