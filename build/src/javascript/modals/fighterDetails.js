import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";
export function showFighterDetailsModal(fighter) {
    const title = "Fighter info";
    let bodyElement = createFighterDetails(fighter);
    showModal(title, bodyElement);
}
function createFighterDetails(fighter) {
    let name = fighter.name;
    let attack = fighter.attack;
    let defense = fighter.defense;
    let health = fighter.defense;
    let image = fighter.source;
    const fighterDetails = createElement("div", "modal-body");
    const nameElement = createElement("p", "fighter-name");
    const attackElement = createElement("p", "fighter-attack");
    const defenseElement = createElement("p", "fighter-defense");
    const healthElement = createElement("p", "fighter-health");
    //const imageElement = createElement("p", "fighter-image");
    // show fighter name, attack, defense, health, image
    nameElement.innerText = name;
    attackElement.innerText = attack;
    defenseElement.innerText = defense;
    healthElement.innerText = health;
    //imageElement.innerHTML = image;
    fighterDetails.append(nameElement);
    fighterDetails.append(attackElement);
    fighterDetails.append(defenseElement);
    fighterDetails.append(healthElement);
    //fighterDetails.append(imageElement);
    return fighterDetails;
}
