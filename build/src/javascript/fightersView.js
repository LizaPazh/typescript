import { createFighter } from "./fighterView";
import { showFighterDetailsModal } from "./modals/fighterDetails";
import { createElement } from "./helpers/domHelper";
import { fight } from "./fight";
import { showWinnerModal } from "./modals/winner";
import { getFighterDetails } from "./services/fightersService";
export function createFighters(fighters) {
    //const selectFighterForBattle = createFightersSelector();
    const fighterElements = fighters.map((fighter) => createFighter(fighter));
    const fightersContainer = createElement("div", "fighters");
    fightersContainer.append(...fighterElements);
    return fightersContainer;
}
const fightersDetailsCache = new Map();
export async function showFighterDetails(event, fighter) {
    let fullInfo = (await getFighterInfo(fighter._id));
    showFighterDetailsModal(fullInfo);
}
export async function getFighterInfo(fighterId) {
    let fighterInfo = (await getFighterDetails(fighterId));
    return fighterInfo;
    // get fighter form fightersDetailsCache or use getFighterDetails function
}
export function createFightersSelector(event, fighter) {
    const selectedFighters = new Map();
    selectFighterForBattle(event, fighter);
    async function selectFighterForBattle(event, fighter) {
        alert(fighter._id);
        const fullInfo = await getFighterInfo(fighter._id);
        if (event.target.checked) {
            selectedFighters.set(fighter._id, fullInfo);
        }
        else {
            selectedFighters.delete(fighter._id);
        }
        if (selectedFighters.size === 2) {
            const winner = fight(...selectedFighters.values());
            showWinnerModal(winner);
        }
    }
}
