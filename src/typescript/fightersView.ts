import { createFighter } from "./fighterView";
import { showFighterDetailsModal } from "./modals/fighterDetails";
import { createElement } from "./helpers/domHelper";
import { fight } from "./fight";
import { showWinnerModal } from "./modals/winner";
import { getFighterDetails } from "./services/fightersService";

export function createFighters(fighters: Fighter[]) {
  const fighterElements = fighters.map((fighter) => createFighter(fighter));
  const fightersContainer = createElement("div", "fighters");

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map<number, FighterDetailsInfo>();

export async function showFighterDetails(event: Event, fighter: Fighter) {
  let fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: number) {
  if (fightersDetailsCache.has(fighterId)) {
    return fightersDetailsCache.get(fighterId);
  }

  let fighterInfo = (await getFighterDetails(fighterId)) as FighterDetailsInfo;
  fightersDetailsCache.set(fighterId, fighterInfo);

  return fighterInfo;
}

const selectedFighters = new Map<number, FighterDetailsInfo>();

export function createFightersSelector(event?: Event, fighter?: Fighter) {
  selectFighterForBattle(event, fighter);
  async function selectFighterForBattle(event: Event, fighter: Fighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>event.target).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const winner = fight(...selectedFighters.values());
      showWinnerModal(winner);
    }
  }
}
