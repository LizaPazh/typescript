export function fight(
  firstFighter?: FighterDetailsInfo,
  secondFighter?: FighterDetailsInfo
): FighterDetailsInfo {
  let firstFighterHealth = firstFighter.health;
  let secondFighterHealth = secondFighter.health;

  while (firstFighterHealth > 0 && secondFighterHealth > 0) {
    secondFighterHealth -= getDamage(firstFighter, secondFighter);
    firstFighterHealth -= getDamage(secondFighter, firstFighter);
  }
  if (secondFighterHealth > 0) return secondFighter;

  return firstFighter;
}

export function getDamage(
  attacker: FighterDetailsInfo,
  enemy: FighterDetailsInfo
): number {
  var damage: number;
  damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter: FighterDetailsInfo): number {
  let criticalHitChance: number = Math.random() * 2 + 1;
  let power: number = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: FighterDetailsInfo): number {
  let dodgeChance: number = Math.random() * 2 + 1;
  let power: number = fighter.defense * dodgeChance;
  return power;
}
