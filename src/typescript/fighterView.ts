import { createElement } from "./helpers/domHelper";
import { showFighterDetails, createFightersSelector } from "./fightersView";

export function createFighter(fighter: Fighter) {
  let name: string = fighter.name;
  let source: string = fighter.source;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox(source);
  const fighterContainer = createElement("div", "fighter");

  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: Event) => ev.stopPropagation();
  const onCheckboxClick = (ev: Event) => createFightersSelector(ev, fighter);
  const onFighterClick = (ev: Event) => showFighterDetails(ev, fighter);

  fighterContainer.addEventListener("click", onFighterClick, false);
  checkboxElement.addEventListener("change", onCheckboxClick, false);
  checkboxElement.addEventListener("click", preventCheckboxClick, false);

  return fighterContainer;
}

function createName(name: string) {
  const nameElement = createElement("span", "name");
  nameElement.innerText = name;

  return nameElement;
}

function createImage(source: string) {
  const attributes = { src: source };
  const imgElement = createElement("img", "fighter-image", attributes);

  return imgElement;
}

function createCheckbox(source: string) {
  const label = createElement("label", "custom-checkbox");
  const span = createElement("span", "checkmark");
  const attributes = { type: "checkbox" };
  const checkboxElement = createElement("input", null, attributes);

  label.append(checkboxElement, span);
  return label;
}
