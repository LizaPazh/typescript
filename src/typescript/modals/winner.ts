import { showModal } from "./modal";
import { createElement } from "../helpers/domHelper";

export function showWinnerModal(fighter: FighterDetailsInfo) {
  const title = "Winner";
  let bodyElement: HTMLElement = createWinnerInfo(fighter);
  showModal(title, bodyElement);
}

function createWinnerInfo(fighter: FighterDetailsInfo) {
  const fighterDetails = createElement("div", "modal-body");
  const nameElement = createElement("p", "fighter-name");
  const image = new Image();
  image.src = fighter.source;

  nameElement.innerText = fighter.name;

  fighterDetails.append(nameElement);
  fighterDetails.append(image);

  return fighterDetails;
}
