import { createElement } from "../helpers/domHelper";
import { showModal } from "./modal";

export function showFighterDetailsModal(fighter: FighterDetailsInfo) {
  const title = "Fighter info";
  let bodyElement: HTMLElement = createFighterDetails(fighter);
  showModal(title, bodyElement);
}

function createFighterDetails(fighter: FighterDetailsInfo) {
  const fighterDetails = createElement("div", "modal-body");
  const nameElement = createElement("p", "fighter-name");
  const attackElement = createElement("p", "fighter-attack");
  const defenseElement = createElement("p", "fighter-defense");
  const healthElement = createElement("p", "fighter-health");
  const image = new Image(150, 200);
  image.src = fighter.source;

  nameElement.innerText = fighter.name;
  attackElement.innerText = `attack: ${fighter.attack.toString()}`;
  defenseElement.innerText = `defense: ${fighter.defense.toString()}`;
  healthElement.innerText = `health: ${fighter.health.toString()}`;

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(image);
  return fighterDetails;
}
